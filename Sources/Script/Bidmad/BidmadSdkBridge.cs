using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using AOT;

public class BidmadSdkBridge : MonoBehaviour
{
#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void _setBannerZoneId(string zoneId);

    [DllImport("__Internal")]
    private static extern void _setInterstitialZoneId(string zoneId);
    
    [DllImport("__Internal")]
    private static extern void _setRewardZoneId(string zoneId);

    [DllImport("__Internal")]
    private static extern void _setRefreshInterval(int time);

    [DllImport("__Internal")]
    private static extern void _setAge(int age);

    [DllImport("__Internal")]
    private static extern void _setIsDebug(bool isDebug);

    [DllImport("__Internal")]
    private static extern void _setLongitude(float longtitude);

    [DllImport("__Internal")]
    private static extern void _setLatitude(float latitude);

    [DllImport("__Internal")]
    private static extern void _setGender(string zoneId);

    [DllImport("__Internal")]
    private static extern void _setKeyword(string zoneId);

    [DllImport("__Internal")]
    private static extern void _setIsPreviewHouse(bool isPreviewHouse);

    [DllImport("__Internal")]
    private static extern void _setCustomHousePath(string housePath);

    [DllImport("__Internal")]
    private static extern void _loadBanner(float _x, float _y);

    [DllImport("__Internal")]
    private static extern void _removeBanner();

    [DllImport("__Internal")]
    private static extern void _loadInterstitial();

    [DllImport("__Internal")]
    private static extern void _showInterstitial();

    [DllImport("__Internal")]
    private static extern void _directShowInterstitialView();
    
    [DllImport("__Internal")]
    private static extern void _loadRewardVideo();
    
    [DllImport("__Internal")]
    private static extern void _showRewardVideo();
    
        [DllImport("__Internal")]
    private static extern void _directShowRewardView();
    
    [DllImport("__Internal")]
    private static extern void _setTestMode(bool b);

#elif UNITY_ANDROID
    private static AndroidJavaObject activityBannerContext = null;
    private static AndroidJavaClass javaBannerClass = null;
    private static AndroidJavaObject javaBannerClassInstance = null;

    private static AndroidJavaObject activityInterstitialContext = null;
    private static AndroidJavaClass javaInterstitialClass = null;
    private static AndroidJavaObject javaInterstitialClassInstance = null;

    private static AndroidJavaObject activityRewardContext = null;
    private static AndroidJavaClass javaRewardClass = null;
    private static AndroidJavaObject javaRewardClassInstance = null;    
#endif

    public static void setRefreshInterval(int time)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setRefreshInterval(time);
        }
        #elif UNITY_ANDROID
            javaBannerClassInstance.Call("setInterval", time);
        #endif
    }

    public static void setIOSBannerZoneId(string zoneId)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setBannerZoneId(zoneId);
        }
        #elif UNITY_ANDROID

        #endif
    }

    public static void setIOSInterstitialZoneId(string zoneId)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setInterstitialZoneId(zoneId);
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void setIOSRewardZoneId(string zoneId)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setRewardZoneId(zoneId);
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void setAndroidBannerZoneId(string zoneId)
    {
        #if UNITY_IOS
        #elif UNITY_ANDROID
            javaBannerClassInstance.Call("setAdInfo", zoneId);
        #endif
    }

    public static void setAndroidInterstitialZoneId(string zoneId)
    {
        #if UNITY_IOS
        #elif UNITY_ANDROID
            javaInterstitialClassInstance.Call("setAdInfo", zoneId);
        #endif
    }

    public static void setAndroidRewardZoneId(string zoneId)
    {
        #if UNITY_IOS
        #elif UNITY_ANDROID
            javaRewardClassInstance.Call("setAdInfo", zoneId);
        #endif
    }

    public static void setAge(int age)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setAge(age);
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void setIsDebug(bool isDebug)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setIsDebug(isDebug);
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void setLongitude(float longitude)
    {
        #if UNITY_IOS
            _setLongitude(longitude);
        #elif UNITY_ANDROID
        #endif
    }

    public static void setLatitude(float latitude)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setLatitude(latitude);
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void setGender(string gender)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setGender(gender);
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void setKeyword(string keyword)
    {
        #if UNITY_IOS
            _setKeyword(keyword);
        #elif UNITY_ANDROID
        #endif
    }

    public static void setIsPreviewHouse(bool isPreviewHouse)
    {
#if UNITY_IOS
        _setIsPreviewHouse(isPreviewHouse);
#elif UNITY_ANDROID
#endif
    }

    public static void setCustomHousePath(string housePath)
    {
#if UNITY_IOS
        _setCustomHousePath(housePath);
#elif UNITY_ANDROID
#endif
    }

    public static void removeBanner()
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _removeBanner();
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void loadIOSBanner(float x, float y)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _loadBanner(x, y);
        }
        #elif UNITY_ANDROID
        #endif
    }

    public static void loadAndroidBanner(int y) {
        #if UNITY_IOS
        #elif UNITY_ANDROID
            javaBannerClassInstance.Call("start",y);
        #endif   
    }

    public static void loadInterstitial()
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _loadInterstitial();
        }
        #elif UNITY_ANDROID
            javaInterstitialClassInstance.Call("load");
        #endif
    }

    public static void showInterstitial()
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _showInterstitial();
        }
        #elif UNITY_ANDROID
            javaInterstitialClassInstance.Call("show");
        #endif
    }

    public static void directShowInterstitialView()
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _directShowInterstitialView();
        }
        #elif UNITY_ANDROID
            javaInterstitialClassInstance.Call("start");
        #endif
    }
    
     public static void setRewardTestMode(bool b)
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _setTestMode(b);
        }
        #elif UNITY_ANDROID
            javaInterstitialClassInstance.Call("start");
        #endif
    }

    public static void showRewardVideo()
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _showRewardVideo();
        }
        #elif UNITY_ANDROID
            javaRewardClassInstance.Call("show");
        #endif
    }
    
    public static void loadRewardVideo()
    {
        #if UNITY_IOS
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            _loadRewardVideo();
        }
        #elif UNITY_ANDROID
            javaRewardClassInstance.Call("load");
        #endif
    }

    public static void directShowRewardView() {
        #if UNITY_IOS
        	_directShowRewardView();
        #elif UNITY_ANDROID
            javaRewardClassInstance.Call("start");
        #endif
    }


#if UNITY_ANDROID
    public static void setHouseImg(string filePath) {
        Texture2D file = Resources.Load(filePath) as Texture2D;
        byte[] imageData = file.EncodeToPNG();
        javaBannerClassInstance.Call("setHouseImg", imageData);
    }

    public static void makeAdView() {
        using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            activityBannerContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        }

        using (javaBannerClass = new AndroidJavaClass("com.adop.sdk.adview.UnityAdView"))
        {
            if(javaBannerClass != null) 
            {
                javaBannerClassInstance = javaBannerClass.CallStatic<AndroidJavaObject>("instance");
                javaBannerClassInstance.Call("setContext", activityBannerContext);
                javaBannerClassInstance.Call("makeAdView");
            }
        }
    }

    public static void makeInterstitial() {
        using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            activityInterstitialContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        }

        using (javaInterstitialClass = new AndroidJavaClass("com.adop.sdk.interstitial.UnityInterstitial"))
        {
            if(javaInterstitialClass != null) 
            {
                javaInterstitialClassInstance = javaInterstitialClass.CallStatic<AndroidJavaObject>("instance");
                javaInterstitialClassInstance.Call("setContext", activityInterstitialContext);      //현재 유니티 화면을 넣는다.
                javaInterstitialClassInstance.Call("makeInterstitial");
            }
        }
    }

    public static void makeReward() {
        using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            activityRewardContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
        }

        using (javaRewardClass = new AndroidJavaClass("com.adop.sdk.reward.UnityReward"))
        {
            if(javaRewardClass != null)
            {
                javaRewardClassInstance = javaRewardClass.CallStatic<AndroidJavaObject>("instance");
                javaRewardClassInstance.Call("setContext", activityRewardContext);
                javaRewardClassInstance.Call("makeReward");
            }

        }
    }
#endif
}
