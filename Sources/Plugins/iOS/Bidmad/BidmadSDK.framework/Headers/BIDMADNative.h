//
//  BIDMADNative.h
//  BidmadSDK
//
//  Created by 김선정 on 21/08/2019.
//  Copyright © 2019 ADOP Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BIDMADNativeAds.h"
#import "BIDMADAdmob.h"
#import "BIDMADInmobi.h"

NS_ASSUME_NONNULL_BEGIN

@protocol BIDMADNativeDelegate;

@interface BIDMADNative : NSObject

@property (nonatomic, strong) id<BIDMADNativeDelegate> delegate;
@property (assign, nonatomic) SEL                       requestSelector;
@property (strong, nonatomic) NSDictionary*             ads_dic;
@property (strong, nonatomic) NSString*                  isFBB;

@property (strong, nonatomic) NSDictionary*                      ecmp_rev_info;
@property (strong, nonatomic) NSDictionary*                      area_info;

@property (strong, nonatomic) NSDictionary*                   change_info;
@property (strong, nonatomic) NSDictionary*                   date;

@property (nonatomic) int                       mediationNumber;

@property (nonatomic) BOOL                      isLabelService;

@property (nonatomic) BOOL                      isLabelServiceAdmin;

@property (nonatomic) float                     arpmYpoint;

- (id)initWithParentViewController:(UIViewController *)parentVC;

-(void) runAds;

- (void) requestNativeView;

- (void)removeAds;

- (void)selectAds:(NSDictionary *)lv_dic isAdsExist:(BOOL)isAds;

@end

@protocol BIDMADNativeDelegate <NSObject>

@required
/// LOADED Native VIEW
- (void)BIDMADNativeLoaded:(BIDMADNativeAds *)nativeAds;

@optional

- (void)BIDMADNativeAllFail:(BIDMADNative *)core;

- (void)BIDMADNativeError:(BIDMADNative *)core code:(NSString *)error;

@end


NS_ASSUME_NONNULL_END
