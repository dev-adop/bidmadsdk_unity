//
//  BIDMADInmobi.h
//  BidmadSDK
//
//  Created by 김선정 on 26/08/2019.
//  Copyright © 2019 ADOP Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BIDMADBanner.h"
#import "BIDMADInterstitial.h"
#import "BIDMADRewardVideo.h"
#import "BIDMADNative.h"
#import "BIDMADSetting.h"
#import "BIDMADUtil.h"
#import "BIDMADLabelView.h"
#import "UIButton+circle.h"
#if __has_include("GoogleMobileAds/GoogleMobileAds.h")
#import <InMobiSDK/InMobiSDK.h>
#define INMOBISDK_EXIST
#endif

@class BIDMADBanner;
@class BIDMADInterstitial;
@class BIDMADRewardVideo;
@class BIDMADNative;

NS_ASSUME_NONNULL_BEGIN

@interface BIDMADInmobi : NSObject<IMNativeDelegate>
@property (strong,nonatomic) BIDMADBanner* banner;
@property (strong,nonatomic) BIDMADInterstitial* interstitial;
@property (strong,nonatomic) BIDMADRewardVideo* rewardVideo;
@property (strong,nonatomic) BIDMADNative* native;

@property(nonatomic,strong) IMNative *InMobiNativeAd;

- (id)initWithAdBanner:(NSDictionary *)dic bidmadbanner:(BIDMADBanner *)banner;

- (id)initWithNative:(NSDictionary *)dic bidmadnative:(BIDMADNative *)native;

- (id)initWithInterstitial:(NSDictionary *)dic bidmadInterstitial:(BIDMADInterstitial *)interstitial;

- (id)initWithRewardVideo:(NSDictionary *)dic bidmadReward:(BIDMADRewardVideo *)rewardVideo;

- (void)showAdmobBanner:(UIViewController *)pvc;
- (void)showAdmobInterstitial:(UIViewController *)pvc;
- (void)showAdmobRewardVideo:(UIViewController *)pvc;
- (void)loadNative:(UIViewController *)pvc;

- (void)removeBannerAds;
- (void)removeInterstitialAds;
//- (void)removeNativeAds;

- (void)gogoInterstitial;
- (void)gogoRewardVideo;

@end

NS_ASSUME_NONNULL_END
