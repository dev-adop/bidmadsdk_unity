//
//  BIDMADNativeAds.h
//  BidmadSDK
//
//  Created by 김선정 on 23/08/2019.
//  Copyright © 2019 ADOP Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BIDMADNativeAds : NSObject
#pragma mark - Must be displayed if available

/// Headline
@property(nonatomic, strong) NSString *headline;

#pragma mark - Recommended to display

/// Text that encourages user to take some action with the ad. For example "Install".
@property(nonatomic, strong) NSString *callToAction;
/// Icon image.
@property(nonatomic, strong) UIImage *icon;
/// Description.
@property(nonatomic, strong) NSString *body;
/// Array of GADNativeAdImage objects.
@property(nonatomic, strong) NSMutableArray<UIImage *> *mainImg;
/// App store rating (0 to 5).
@property(nonatomic, strong) NSDecimalNumber *starRating;
/// The app store name. For example, "App Store".
@property(nonatomic, strong) NSString *store;
/// String representation of the app's price.
@property(nonatomic, strong) NSString *price;
/// Identifies the advertiser. For example, the advertiser’s name or visible URL.
@property(nonatomic, strong) NSString *advertiser;
/// Video controller for controlling video playback in GADUnifiedNativeAdView's mediaVi
@property(nonatomic, strong) UIView *nativeAdview;

@end

NS_ASSUME_NONNULL_END
