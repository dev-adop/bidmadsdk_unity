//
//  BIDMADUnityBridge.m
//  BidmadSDK
//
//  Created by 김선정 on 2018. 8. 10..
//  Copyright © 2018년 ADOP Co., Ltd. All rights reserved.
//
#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t pred = 0; \
__strong static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
_sharedObject = block(); \
}); \
return _sharedObject; \

#import "BidmadUnityBridge.h"

@interface BidmadUnityBridge : NSObject<BIDMADBannerDelegate,BIDMADInterstitialDelegate,BIDMADRewardVideoDelegate>
+ (BidmadUnityBridge *)sharedInstance;
@end

static bool isTestMode;

@implementation BidmadUnityBridge

+ (BidmadUnityBridge *)sharedInstance{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        isTestMode = false;
        return [[self alloc]init];
    });
}

-(void)BIDMADBannerClosed:(BIDMADBanner *)core{
    UnitySendMessage("BidmadManager", "OnIosBannerClosed", [@"BIDMADBannerClosed" UTF8String]);
}

-(void)BIDMADBannerLoad:(BIDMADBanner *)core{
    UnitySendMessage("BidmadManager", "OnIosBannerLoaded", [@"BIDMADBannerLoad" UTF8String]);
}

-(void)BIDMADBannerShow:(BIDMADBanner *)core{
    UnitySendMessage("BidmadManager", "OnIosBannerShow", [@"BIDMADBannerShow" UTF8String]);
}

- (void)BIDMADBannerError:(BIDMADBanner *)core code:(NSString *)error{
    UnitySendMessage("BidmadManager", "OnIosBannerError", [error UTF8String]);
}

- (void)BIDMADBannerAllFail:(BIDMADBanner *)core{
    UnitySendMessage("BidmadManager", "OnIosBannerAllFail", [@"BIDMADBannerAllFail" UTF8String]);
}

-(void)BIDMADInterstitialLoad:(BIDMADInterstitial *)core{
    UnitySendMessage("BidmadManager", "OnIosInterstitialLoaded", [@"BIDMADInterstitialLoad" UTF8String]);
}

-(void)BIDMADInterstitialShow:(BIDMADInterstitial *)core{
    UnitySendMessage("BidmadManager", "OnIosInterstitialShow", [@"BIDMADInterstitialShow" UTF8String]);
}

-(void)BIDMADInterstitialClose:(BIDMADInterstitial *)core{
    UnitySendMessage("BidmadManager", "OnIosInterstitialClosed", [@"BIDMADInterstitialClose" UTF8String]);
}

-(void)BIDMADInterstitialError:(BIDMADInterstitial *)core code:(NSString *)error{
    UnitySendMessage("BidmadManager", "OnIosInterstitialError", [error UTF8String]);
}

- (void)BIDMADInterstitialAllFail:(BIDMADInterstitial *)core{
    UnitySendMessage("BidmadManager", "OnIosInterstitialAllFail", [@"BIDMADInterstitialAllFail" UTF8String]);
}

-(void)BIDMADRewardVideoComplete:(BIDMADRewardVideo *)core{
    UnitySendMessage("BidmadManager", "OnIosRewardComplete", [@"BIDMADRewardVideoComplete" UTF8String]);
}

-(void)BIDMADRewardVideoClick:(BIDMADRewardVideo *)core{
    UnitySendMessage("BidmadManager", "OnRewardClicked", [@"BIDMADRewardVideoClick" UTF8String]);
}

-(void)BIDMADRewardVideoClose:(BIDMADRewardVideo *)core{
    UnitySendMessage("BidmadManager", "OnRewardClosed", [@"BIDMADRewardVideoClose" UTF8String]);
}

- (void)BIDMADRewardVideoShow:(BIDMADRewardVideo *)core{
    UnitySendMessage("BidmadManager", "OnRewardOpened", [@"BIDMADRewardVideoShow" UTF8String]);
}

-(void)BIDMADRewardVideoLoad:(BIDMADRewardVideo *)core
{
    UnitySendMessage("BidmadManager", "OnRewardLoaded", [@"BIDMADRewardVideoLoad" UTF8String]);
}

-(void)BIDMADRewardVideoSucceed:(BIDMADRewardVideo *)core
{
    UnitySendMessage("BidmadManager", "OnRewardCompleted", [@"BIDMADRewardVideoSucceed" UTF8String]);
}

-(void)BIDMADRewardVideoError:(BIDMADRewardVideo *)core code:(NSString *)error{
    UnitySendMessage("BidmadManager", "OnIosRewardError", [error UTF8String]);
}

- (void)BIDMADRewardVideoAllFail:(BIDMADRewardVideo *)core{
    UnitySendMessage("BidmadManager", "OnRewardError", [@"BIDMADRewardVideoAllFail" UTF8String]);
}

@end

static BIDMADBanner* __banner= nil;
static BIDMADInterstitial* __interstitial= nil;
static BIDMADRewardVideo* __reward= nil;

void _setRefreshInterval(int time){
    [[BIDMADSetting sharedInstance] setRefreshInterval:time];
}

void _setIsDebug(bool isDebug) {
    [[BIDMADSetting sharedInstance] setIsDebug:isDebug];
}

void _setInterstitialZoneId(const char* zoneId) {
    NSString* zoneID = [NSString stringWithUTF8String:zoneId];
    [[BIDMADSetting sharedInstance] setInterstitialZoneID:zoneID];
}

void _setBannerZoneId(const char* zoneId) {
    NSString* zoneID = [NSString stringWithUTF8String:zoneId];
    [[BIDMADSetting sharedInstance] setBannerZoneID:zoneID];
}

void _setRewardZoneId(const char* zoneId) {
    NSString* zoneID = [NSString stringWithUTF8String:zoneId];
    [[BIDMADSetting sharedInstance] setRewardZoneID:zoneID];
}

void _setAge(int age) {
    [[BIDMADSetting sharedInstance] setAge:age];
}

void _setLongitude(float longtitude) {
    [[BIDMADSetting sharedInstance] setLongitude:longtitude];
}

void _setLatitude(float latitude) {
    [[BIDMADSetting sharedInstance] setLatitude:latitude];
}

void _setGender(const char* _gender) {
    NSString* gender = [NSString stringWithUTF8String:_gender];
    [[BIDMADSetting sharedInstance] setGender:gender];
}

void _setKeyword(const char* _keyword) {
    NSString* keyword = [NSString stringWithUTF8String:_keyword];
    [[BIDMADSetting sharedInstance] setKeyword:keyword];
}

void _setIsPreviewHouse(bool isPreviewHouse){
    [BIDMADSetting sharedInstance].isPreviewHouse = isPreviewHouse;
}

void _setCustomHousePath(const char* housePath){
    NSString* path = [NSString stringWithUTF8String:housePath];
    [[BIDMADSetting sharedInstance] setHouseBannerPath:path];
}

void _removeBanner(){
    [__banner removeAds];
}

void _loadBanner(float _x,float _y){
    UIViewController* pRootViewController = UnityGetGLViewController();
    __banner = [[BIDMADBanner alloc] initWithParentViewController:pRootViewController adsPosition:CGPointMake(_x,_y)];
    [__banner setDelegate:[BidmadUnityBridge sharedInstance]];     //델리게이트 설정;
    [pRootViewController.view addSubview:__banner];    // 광고배너를 유저 화면에 삽입
    [__banner requestBannerView];    // 광고 요청
}

void _loadInterstitial(){
    UIViewController* pRootViewController = UnityGetGLViewController();
    __interstitial = [[BIDMADInterstitial alloc]init];
    [__interstitial setParentViewController:pRootViewController];
    [__interstitial setDelegate:[BidmadUnityBridge sharedInstance]];
    [__interstitial loadInterstitialView];
}

void _showInterstitial(){
    [__interstitial showInterstitialView];
    
}

void _directShowInterstitialView(){
    UIViewController* pRootViewController = UnityGetGLViewController();
    __interstitial = [[BIDMADInterstitial alloc]init];
    [__interstitial setParentViewController:pRootViewController];
    [__interstitial directShowInterstitialView];   //바로 호출
}

void _loadRewardVideo(){
    UIViewController* pRootViewController = UnityGetGLViewController();
    __reward = [[BIDMADRewardVideo alloc]init];
    [__reward setParentViewController:pRootViewController];
    [__reward setDelegate:[BidmadUnityBridge sharedInstance]];
    [__reward loadRewardVideo];
}

void _showRewardVideo(){
    [__reward showRewardVideo];
}

void _setTestMode(bool isDebug) {
    isTestMode = isDebug;
}

void _directShowRewardView(){
    UIViewController* pRootViewController = UnityGetGLViewController();
    __reward = [[BIDMADRewardVideo alloc]init];
    [__reward setParentViewController:pRootViewController];
    [__reward setDelegate:[BidmadUnityBridge sharedInstance]];
    [__reward setTestMode:isTestMode];
    [__reward directShowRewardView];
}
