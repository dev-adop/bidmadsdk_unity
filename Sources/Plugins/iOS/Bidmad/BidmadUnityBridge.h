//
//  BidmadUnityBridge.h
//  BidmadSDK
//
//  Created by 김선정 on 2018. 9. 13..
//  Copyright © 2018년 ADOP Co., Ltd. All rights reserved.
//

#ifndef BidmadUnityBridge_h
#define BidmadUnityBridge_h

#import "BidmadSDK/BIDMADSetting.h"
#import "BidmadSDK/BIDMADBanner.h"
#import "BidmadSDK/BIDMADInterstitial.h"

#ifdef __cplusplus
extern "C" {
#endif
    UIViewController* UnityGetGLViewController();
    void UnitySendMessage(const char* obj, const char* method, const char* msg);
    void _setRefreshInterval(int time);
    void _setInterstitialZoneId(const char* zoneId);
    void _setBannerZoneId(const char* zoneId);
    void _setRewardZoneId(const char* zoneId);
    void _setAge(int age);
    void _setIsDebug(bool isDebug);
    void _setTestMode(bool isDebug);
    void _setLongitude(float longtitude);
    void _setLatitude(float latitude);
    void _setGender(const char* _gender);
    void _setKeyword(const char* _keyword);
    void _setIsPreviewHouse(bool isPreviewHouse);
    void _setCustomHousePath(const char* housePath);
    
    void _loadBanner(float _x,float _y);
    void _loadInterstitial();
    void _showInterstitial();
    void _directShowInterstitialView();
    void _removeBanner();
    void _loadRewardVideo();
    void _showRewardVideo();
    void _directShowRewardView();
    
#ifdef __cplusplus
}
#endif

#endif /* BidmadUnityBridge_h */
